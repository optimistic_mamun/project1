		<?php include("second_header.php"); ?>

<div class="container">
<div id="teacher">
		<div class="image_section">
		<div class="image1">
		<img src="images/MAH.jpg" width="260PX" height="250PX">
		</div>
		
		<div class="content">
		<h2 align="right">Md. Anwar Hossain</h2>
		<p align="right">Assistant Professor &</br>
			Chairman of the Department</br>
			B.Sc(Hons.) and M.Sc in ICE (RU)</br>
			Research area: Communication Engineering</br>
			Contact: 073164986 (Office), +8801717330923</br>
			E-mail: manwar_ice@yahoo.com; manwar_ice@ice.pust.ac.bd
		</p>
		</div>
		</div>
		<div class="image_section">
		<div class="image1">
		<img src="images/PKP.jpg" width="260PX" height="250PX">
		</div>
		<div class="content">
		<h2 align="right">Pallab Kanti Podder</h2>
		<p align="right">Assistant Professor (on Study Leave)</br>
			B.Sc(Hons.) and M.Sc in ICE (IU)</br>
			Research area: Efficient Video Coding</br>
			Contact: +61263384053 (Office), +61424632325, +8801917485890</br>
			E-mail: pallab_ice@yahoo.com; ppodder@csu.edu.au
		</p>
		</div>
		</div>
		<div class="image_section">
		<div class="image1">
		<img src="images/MOF.jpg" width="260PX" height="250PX">
		</div>
		<div class="content">
		<h2 align="right">Dr. Md. Omar Faruk</h2>
		<p align="right">Assistant Professor</br>
			Ph.D (RU)</br>
			Research area: Signal Processing</br>
			Contact: 073164986 (Office), +8801712415335</br>
			E-mail: fom_06@yahoo.com; fom_06@ice.pust.ac.bd
		</p>
		</div>
		</div>
		<div class="image_section">
		<div class="image1">
		<img src="images/AFMZA.jpg" width="260PX" height="250PX">
		</div>
		<div class="content">
		<h2 align="right">Abul Fazal Mohammad Zainul Abadin</h2>
		<p align="right">Assistant Professor</br>
			B.Sc(Hons.) and M.Sc in ICE (RU)</br>
			Research area: Communication Engineering</br>
			Contact: 073164986 (Office), +8801717797217</br>
			E-mail: abadin.7@gmail.com; abadin.7@ice.pust.ac.bd
		</p>
		</div>
		</div>
		<div class="image_section">
		<div class="image1">
		<img src="images/MIH.jpg" width="260PX" height="250PX">
		</div>
		<div class="content">
		<h2 align="right">Md. Imran Hossain</h2>
		<p align="right">Assistant Professor</br>
			B.Sc(Hons.) and M.Sc in ICE (IU)</br>
			Research area: Image Processing</br>
			Contact: 073164986 (Office), +8801759320341</br>
			E-mail: imran05ice@gmail.com; imran05ice@ice.pust.ac.bd
		</p>
		</div>
		</div>
		<div class="image_section">
		<div class="image1">
		<img src="images/IA.jpg" width="260PX" height="250PX">
		</div>
		<div class="content">
		<h2 align="right">Iffat Ara</h2>
		<p align="right">Assistant Professor</br>
			B.Sc(Hons.) and M.Sc in APEE (RU)</br>
			Research area: Signal Processing</br>
			Office: 073164986 (Office), +8801913699368</br>
			E-mail: ara.iffat@ymail.com
		</p>
		</div>
		</div>
		<div class="image_section">
		<div class="image1">
		<img src="images/muntasir.jpg" width="260PX" height="250PX">
		</div>
		<div class="content">
		<h2 align="right">Muntasir Ahmed</h2>
		<p align="right">Lecturer (on Study Leave)</br>
			B.Sc(Hons.) and M.Sc in ICE (RU)</br>
			Research area: Communication Engineering</br>
			Direct Line: 073164986 (Office), +8801718936122</br>
			E-mail: muntasir_ice_ru@yahoo.com 
		</p>
		</div>
		</div>
		<div class="image_section">
		<div class="image1">
		<img src="images/SS.jpg" width="260PX" height="250PX">
		</div>
		<div class="content">
		<h2 align="right">Sohag Sarker</h2>
		<p align="right">Assistant Professor</br>
			B.Sc(Hon's) & M.Sc. in ICE (RU)</br>
			Research area: Communication Engineering</br>
			Direct Line: 073164986 (Office), +8801722456324</br>
			E-mail: sohagsarker5614@gmail.com
		</p>
		</div>
		</div>
		<div class="image_section">
		<div class="image1">
		<img src="images/MSH.jpg" width="260PX" height="250PX">
		</div>
		<div class="content">
		<h2 align="right">Md. Sarwar Hosain</h2>
		<p align="right">Assistant Professor</br>
				B.Sc(Hon's) & M.Sc. in ICE (RU)</br>
				Research area: Wireless Communication</br>
				Direct Line: 073164986 (Office), +8801722047833, +8801913815311</br>
				E-mail: sarwar.iceru@gmail.com; sarwar@ice.pust.ac.bd
		</p>
		</div>
		</div>
		</div>
		</div>

		</body>
		</html>