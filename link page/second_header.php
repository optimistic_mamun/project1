<html lang="en">
	<head>
		<title>Department of ICE at PUST</title>
	
		<meta charset="utf-8" />
		<link rel="shortcut icon" href="../images/iceLogo.PNG">
		<link rel="stylesheet" href="../css/style.css" type="text/css" />
		<link rel="stylesheet" href="../css/menu.css" type="text/css" />
		
		<link rel="stylesheet" href="../default.css" type="text/css" media="screen" />

		 		<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="../css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="../js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
			
		
		
	</head>
	<body  id="body">
	<div class="container">
		<div id ="header1">
		<div class="under">
			<img class="rside"  src="../images/ICE_Header.png"  width="830" height="100" alt="baner Logo"/>
			
			<img class="rside"  src="../images/PUST_logo.jpg"  width="150" height="100" alt="ICE Logo"/>
			
			
		</div>
		</div>
		
		<!-- menu section -->
		
		<div class="container">
		<ul id="nav">
			<div id="side">
			<li><a href="../index.php">Home</a> </li>
			<li><a href="about.php">About ICE</a> </li>
			<li><a href="../ice_session.php">Session</a>
				
			</li>
			
			<li><a href="../teacher.php">Teachers</a>
				
			</li>
			<li><a href="#">Research</a> 
				<ul class="subs">
					<li><a href="thesis.php">Thesis Area</a></li>
					<li><a href="project.php">Project Area</a></li>


				</ul>
			</li>
			<li><a href="../result.php">Result</a> </li>
			<li><a href="facility.php">Facilities</a> </li>
			<li><a href="galary.php">Gallary</a> 
			</li>
			</div>
			</ul>
			</div>
			</div>