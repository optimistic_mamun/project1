<?php include("second_header.php"); ?>

<html>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="description" content="Dream Slider a jQuery treat" />
        <meta name="keywords" content="jquery, dreamslider, slider, effect, images, photos, animation, background-image"/>
        <link rel="stylesheet" href="../galary/dist/css/dreamslider.min.css" type="text/css" media="screen"/>
        <link rel="stylesheet" href="style.css" type="text/css" media="screen"/>
        <link rel="stylesheet" href="../css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="../css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="../js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    </head>

    <body>
    <div class="container">
        <h1 class="dreamSlider_title" style="margin-left: 150px;">Information and Communication Engineering</h1>
        <div class="container">
           
            <div class="im_wrapper" style="position:absolute; margin-left:-100px ">

                <div ><img src="img/1.jpg" alt="" /></div>
                <div ><img src="img/2.jpg" alt="" /></div>
                <div ><img src="img/3.jpg" alt="" /></div>
                <div ><img src="img/4.jpg" alt="" /></div>
                <div ><img src="img/5.jpg" alt="" /></div>
                <div ><img src="img/6.jpg" alt="" /></div>

                <div ><img src="img/7.jpg" alt="" /></div>
                <div ><img src="img/8.jpg" alt="" /></div>
                <div ><img src="img/9.jpg" alt="" /></div>
                <div ><img src="img/10.jpg" alt="" /></div>
                <div ><img src="img/11.jpg" alt="" /></div>
                <div ><img src="img/12.jpg" alt="" /></div>

                <div ><img src="img/13.jpg" alt="" /></div>
                <div ><img src="img/14.jpg" alt="" /></div>
                <div ><img src="img/15.jpg" alt="" /></div>
                <div ><img src="img/16.jpg" alt="" /></div>
                <div ><img src="img/17.jpg" alt="" /></div>
                <div ><img src="img/18.jpg" alt="" /></div>

                <div ><img src="img/19.jpg" alt="" /></div>
                <div ><img src="img/20.jpg" alt="" /></div>
                <div ><img src="img/21.jpg" alt="" /></div>
                <div ><img src="img/22.jpg" alt="" /></div>
                <div ><img src="img/23.jpg" alt="" /></div>
                <div ><img src="img/24.jpg" alt="" /></div>

                <div ><img src="img/25.jpg" alt="" /></div>
                <div ><img src="img/26.jpg" alt="" /></div>
                <div ><img src="img/27.jpg" alt="" /></div>
                <div ><img src="img/28.jpg" alt="" /></div>
                <div ><img src="img/29.jpg" alt="" /></div>
                <div ><img src="img/30.jpg" alt="" /></div>

                <div ><img src="img/31.jpg" alt="" /></div>
                <div ><img src="img/32.jpg" alt="" /></div>
                <div ><img src="img/33.jpg" alt="" /></div>
                <div ><img src="img/34.jpg" alt="" /></div>
                <div ><img src="img/35.jpg" alt="" /></div>
                <div ><img src="img/36.jpg" alt="" /></div>

                <div ><img src="img/37.jpg" alt="" /></div>
                <div ><img src="img/38.jpg" alt="" /></div>
                <div ><img src="img/39.jpg" alt="" /></div>
                <div ><img src="img/40.jpg" alt="" /></div>
                <div ><img src="img/41.jpg" alt="" /></div>
                <div ><img src="img/42.jpg" alt="" /></div>
            </div>

        

            
        </div>
        </div>
       
        <!-- The JavaScript libraries section-->
        <script type="text/javascript" src="../galary/js/jquery.js"></script>
        <script type="text/javascript" src="../galary/dist/js/dreamslider.min.js"></script>
        <script type="text/javascript">
            $(function(){
                $('.container').dreamSlider({
                    rowCount:6
                    //,easeEffect: 'bounce'
                    //,easeEffect: 'standOut'
                });
            });
        </script>
    </body>
</html>
