-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 19, 2016 at 07:08 AM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ice_dept`
--

-- --------------------------------------------------------

--
-- Table structure for table `ice`
--

CREATE TABLE IF NOT EXISTS `ice` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `roll` int(10) NOT NULL,
  `name` text NOT NULL,
  `sub1` float NOT NULL,
  `sub2` float NOT NULL,
  `sub3` float NOT NULL,
  `sub4` float NOT NULL,
  `sub5` float NOT NULL,
  `sub6` float NOT NULL,
  `sub7` float NOT NULL,
  `sub8` float NOT NULL,
  `sub9` float NOT NULL,
  `sub10` float NOT NULL,
  `CGPA` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roll` (`roll`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `ice`
--

INSERT INTO `ice` (`id`, `roll`, `name`, `sub1`, `sub2`, `sub3`, `sub4`, `sub5`, `sub6`, `sub7`, `sub8`, `sub9`, `sub10`, `CGPA`) VALUES
(5, 110628, 'MD. ABDULLAH-AL-MAMUN', 56, 75, 68, 80, 64, 75, 75, 62, 65, 80, 3.35);

-- --------------------------------------------------------

--
-- Table structure for table `ice_1_2`
--

CREATE TABLE IF NOT EXISTS `ice_1_2` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `roll` int(10) NOT NULL,
  `name` text NOT NULL,
  `sub1` float NOT NULL,
  `sub2` float NOT NULL,
  `sub3` float NOT NULL,
  `sub4` float NOT NULL,
  `sub5` float NOT NULL,
  `sub6` float NOT NULL,
  `sub7` float NOT NULL,
  `sub8` float NOT NULL,
  `sub9` float NOT NULL,
  `CGPA` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roll` (`roll`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ice_1_2`
--

INSERT INTO `ice_1_2` (`id`, `roll`, `name`, `sub1`, `sub2`, `sub3`, `sub4`, `sub5`, `sub6`, `sub7`, `sub8`, `sub9`, `CGPA`) VALUES
(1, 110628, 'MD. ABDULLAH-AL-MAMUN', 81, 75, 84, 75, 65, 76, 65, 65, 54, 3.56);

-- --------------------------------------------------------

--
-- Table structure for table `ice_2_1`
--

CREATE TABLE IF NOT EXISTS `ice_2_1` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `roll` int(10) NOT NULL,
  `name` text NOT NULL,
  `sub1` float NOT NULL,
  `sub2` float NOT NULL,
  `sub3` float NOT NULL,
  `sub4` int(11) NOT NULL,
  `sub5` float NOT NULL,
  `sub6` float NOT NULL,
  `sub7` float NOT NULL,
  `sub8` float NOT NULL,
  `sub9` float NOT NULL,
  `sub10` float NOT NULL,
  `CGPA` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roll` (`roll`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ice_2_1`
--

INSERT INTO `ice_2_1` (`id`, `roll`, `name`, `sub1`, `sub2`, `sub3`, `sub4`, `sub5`, `sub6`, `sub7`, `sub8`, `sub9`, `sub10`, `CGPA`) VALUES
(1, 110628, 'MD. ABDULLAH-AL-MAMUN', 52, 86, 75, 65, 75, 67, 75, 45, 84, 66, 3.42);

-- --------------------------------------------------------

--
-- Table structure for table `ice_2_2`
--

CREATE TABLE IF NOT EXISTS `ice_2_2` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `roll` int(10) NOT NULL,
  `name` text NOT NULL,
  `sub1` float NOT NULL,
  `sub2` float NOT NULL,
  `sub3` float NOT NULL,
  `sub4` float NOT NULL,
  `sub5` float NOT NULL,
  `sub6` float NOT NULL,
  `sub7` float NOT NULL,
  `sub8` float NOT NULL,
  `sub9` float NOT NULL,
  `sub10` float NOT NULL,
  `CGPA` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roll` (`roll`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ice_2_2`
--

INSERT INTO `ice_2_2` (`id`, `roll`, `name`, `sub1`, `sub2`, `sub3`, `sub4`, `sub5`, `sub6`, `sub7`, `sub8`, `sub9`, `sub10`, `CGPA`) VALUES
(1, 110628, 'MD. ABDULLAH-AL-MAMUN', 65, 75, 68, 72, 78, 82, 75, 54, 75, 66, 3.35);

-- --------------------------------------------------------

--
-- Table structure for table `ice_3_1`
--

CREATE TABLE IF NOT EXISTS `ice_3_1` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `roll` int(10) NOT NULL,
  `name` text NOT NULL,
  `sub1` float NOT NULL,
  `sub2` float NOT NULL,
  `sub3` float NOT NULL,
  `sub4` float NOT NULL,
  `sub5` float NOT NULL,
  `sub6` float NOT NULL,
  `sub7` float NOT NULL,
  `sub8` float NOT NULL,
  `sub9` float NOT NULL,
  `sub10` float NOT NULL,
  `sub11` float NOT NULL,
  `CGPA` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roll` (`roll`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ice_3_1`
--


-- --------------------------------------------------------

--
-- Table structure for table `ice_3_2`
--

CREATE TABLE IF NOT EXISTS `ice_3_2` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `roll` int(10) NOT NULL,
  `name` text NOT NULL,
  `sub1` float NOT NULL,
  `sub2` float NOT NULL,
  `sub3` float NOT NULL,
  `sub4` float NOT NULL,
  `sub5` float NOT NULL,
  `sub6` float NOT NULL,
  `sub7` float NOT NULL,
  `sub8` float NOT NULL,
  `sub9` float NOT NULL,
  `sub10` float NOT NULL,
  `sub11` float NOT NULL,
  `CGPA` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roll` (`roll`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ice_3_2`
--


-- --------------------------------------------------------

--
-- Table structure for table `ice_4_1`
--

CREATE TABLE IF NOT EXISTS `ice_4_1` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `roll` int(10) NOT NULL,
  `name` text NOT NULL,
  `sub1` float NOT NULL,
  `sub2` float NOT NULL,
  `sub3` float NOT NULL,
  `sub4` float NOT NULL,
  `sub5` float NOT NULL,
  `sub6` float NOT NULL,
  `sub7` float NOT NULL,
  `sub8` float NOT NULL,
  `sub9` float NOT NULL,
  `sub10` float NOT NULL,
  `CGPA` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roll` (`roll`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ice_4_1`
--


-- --------------------------------------------------------

--
-- Table structure for table `ice_4_2`
--

CREATE TABLE IF NOT EXISTS `ice_4_2` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `roll` int(10) NOT NULL,
  `name` text NOT NULL,
  `sub1` float NOT NULL,
  `sub2` float NOT NULL,
  `sub3` float NOT NULL,
  `sub4` float NOT NULL,
  `sub5` float NOT NULL,
  `sub6` float NOT NULL,
  `sub7` float NOT NULL,
  `sub8` float NOT NULL,
  `sub9` float NOT NULL,
  `sub10` float NOT NULL,
  `sub11` float NOT NULL,
  `CGPA` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roll` (`roll`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ice_4_2`
--


-- --------------------------------------------------------

--
-- Table structure for table `reg_info`
--

CREATE TABLE IF NOT EXISTS `reg_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text NOT NULL,
  `F_name` text NOT NULL,
  `M_name` text NOT NULL,
  `session` varchar(60) NOT NULL,
  `roll` varchar(6) NOT NULL,
  `reg` varchar(10) NOT NULL,
  `profile_image` varchar(500) NOT NULL,
  `contact_number` varchar(15) NOT NULL,
  `email` varchar(25) NOT NULL,
  `user_id` varchar(15) NOT NULL,
  `pass` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roll` (`roll`,`reg`,`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `reg_info`
--

INSERT INTO `reg_info` (`id`, `Name`, `F_name`, `M_name`, `session`, `roll`, `reg`, `profile_image`, `contact_number`, `email`, `user_id`, `pass`) VALUES
(18, 'MD. ABDULLAH-AL-MAMUN', 'Md. Mashiur Rahman', 'Momotaz Begum', '2010-2011', '110628', '1065028', 'images/IMG_2077.JPG', '01758466351', 'mamun.icepust@yahoo.com', 'ice_mamun', '123456789');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
