-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 29, 2015 at 04:58 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ice_dept`
--

-- --------------------------------------------------------

--
-- Table structure for table `ice`
--

CREATE TABLE IF NOT EXISTS `ice` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `roll` int(10) NOT NULL,
  `name` text NOT NULL,
  `sub1` float NOT NULL,
  `sub2` float NOT NULL,
  `sub3` float NOT NULL,
  `sub4` float NOT NULL,
  `sub5` float NOT NULL,
  `sub6` float NOT NULL,
  `sub7` float NOT NULL,
  `sub8` float NOT NULL,
  `sub9` float NOT NULL,
  `sub10` float NOT NULL,
  `CGPA` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roll` (`roll`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ice`
--


-- --------------------------------------------------------

--
-- Table structure for table `ice_1_2`
--

CREATE TABLE IF NOT EXISTS `ice_1_2` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `roll` int(10) NOT NULL,
  `name` text NOT NULL,
  `sub1` float NOT NULL,
  `sub2` float NOT NULL,
  `sub3` float NOT NULL,
  `sub4` float NOT NULL,
  `sub5` float NOT NULL,
  `sub6` float NOT NULL,
  `sub7` float NOT NULL,
  `sub8` float NOT NULL,
  `sub9` float NOT NULL,
  `CGPA` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roll` (`roll`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ice_1_2`
--


-- --------------------------------------------------------

--
-- Table structure for table `ice_2_1`
--

CREATE TABLE IF NOT EXISTS `ice_2_1` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `roll` int(10) NOT NULL,
  `name` text NOT NULL,
  `sub1` float NOT NULL,
  `sub2` float NOT NULL,
  `sub3` float NOT NULL,
  `sub4` int(11) NOT NULL,
  `sub5` float NOT NULL,
  `sub6` float NOT NULL,
  `sub7` float NOT NULL,
  `sub8` float NOT NULL,
  `sub9` float NOT NULL,
  `sub10` float NOT NULL,
  `CGPA` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roll` (`roll`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ice_2_1`
--


-- --------------------------------------------------------

--
-- Table structure for table `ice_2_2`
--

CREATE TABLE IF NOT EXISTS `ice_2_2` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `roll` int(10) NOT NULL,
  `name` text NOT NULL,
  `sub1` float NOT NULL,
  `sub2` float NOT NULL,
  `sub3` float NOT NULL,
  `sub4` float NOT NULL,
  `sub5` float NOT NULL,
  `sub6` float NOT NULL,
  `sub7` float NOT NULL,
  `sub8` float NOT NULL,
  `sub9` float NOT NULL,
  `sub10` float NOT NULL,
  `CGPA` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roll` (`roll`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ice_2_2`
--


-- --------------------------------------------------------

--
-- Table structure for table `ice_3_1`
--

CREATE TABLE IF NOT EXISTS `ice_3_1` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `roll` int(10) NOT NULL,
  `name` text NOT NULL,
  `sub1` float NOT NULL,
  `sub2` float NOT NULL,
  `sub3` float NOT NULL,
  `sub4` float NOT NULL,
  `sub5` float NOT NULL,
  `sub6` float NOT NULL,
  `sub7` float NOT NULL,
  `sub8` float NOT NULL,
  `sub9` float NOT NULL,
  `sub10` float NOT NULL,
  `sub11` float NOT NULL,
  `CGPA` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roll` (`roll`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ice_3_1`
--


-- --------------------------------------------------------

--
-- Table structure for table `ice_3_2`
--

CREATE TABLE IF NOT EXISTS `ice_3_2` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `roll` int(10) NOT NULL,
  `name` text NOT NULL,
  `sub1` float NOT NULL,
  `sub2` float NOT NULL,
  `sub3` float NOT NULL,
  `sub4` float NOT NULL,
  `sub5` float NOT NULL,
  `sub6` float NOT NULL,
  `sub7` float NOT NULL,
  `sub8` float NOT NULL,
  `sub9` float NOT NULL,
  `sub10` float NOT NULL,
  `sub11` float NOT NULL,
  `CGPA` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roll` (`roll`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ice_3_2`
--


-- --------------------------------------------------------

--
-- Table structure for table `ice_4_1`
--

CREATE TABLE IF NOT EXISTS `ice_4_1` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `roll` int(10) NOT NULL,
  `name` text NOT NULL,
  `sub1` float NOT NULL,
  `sub2` float NOT NULL,
  `sub3` float NOT NULL,
  `sub4` float NOT NULL,
  `sub5` float NOT NULL,
  `sub6` float NOT NULL,
  `sub7` float NOT NULL,
  `sub8` float NOT NULL,
  `sub9` float NOT NULL,
  `sub10` float NOT NULL,
  `CGPA` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roll` (`roll`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ice_4_1`
--


-- --------------------------------------------------------

--
-- Table structure for table `ice_4_2`
--

CREATE TABLE IF NOT EXISTS `ice_4_2` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `roll` int(10) NOT NULL,
  `name` text NOT NULL,
  `sub1` float NOT NULL,
  `sub2` float NOT NULL,
  `sub3` float NOT NULL,
  `sub4` float NOT NULL,
  `sub5` float NOT NULL,
  `sub6` float NOT NULL,
  `sub7` float NOT NULL,
  `sub8` float NOT NULL,
  `sub9` float NOT NULL,
  `sub10` float NOT NULL,
  `sub11` float NOT NULL,
  `CGPA` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roll` (`roll`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ice_4_2`
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
