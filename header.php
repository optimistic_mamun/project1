<!DOCTYPE  HTML>
<html lang="en">
	<head>
		<title>Department of ICE at PUST</title>
	
		<meta charset="utf-8" />
		<link rel="shortcut icon" href="images/iceLogo.PNG">
		<link rel="stylesheet" href="css/style.css" type="text/css" />
		<link rel="stylesheet" href="css/menu.css" type="text/css" />
		<link rel="stylesheet" href="css/slide.css" type="text/css" />
		
		<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
				
		<script type="text/javascript" src="JavaScript/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="JavaScript/jquery.slidertron-1.1.js"></script>
		<script type="text/javascript">
			$(function(){
					$('#slider').slidertron({
					viewerSelector: '.viewer',
					reelSelector: '.reel',
					slidesSelector: '.slide',
					advanceDelay: 3000,
					speed: 'slow'
				});			
			});
			
		</script>
		
		<script>
			
		 	$(function(){
				$('span').click(function(){
					if($('#hide').css('display') =='none'){
						$('#hide').css({display:'block'});
						$('span').html('Less&raquo;&raquo;');
						
					}else{
						$('#hide').css('display','none');
						$('span').html('More&raquo;&raquo;');
					}
				});
				$('hide').css('display','none');
			});
			
		</script>
		
		
	</head>
	<body id="body">
	<div class="container">
		<div id ="header1">
		<div class="under">
			<img class="rside"  src="images/ICE_Header.png"  width="830" height="100" alt="baner Logo"/>
			
			<img class="rside"  src="images/PUST_logo.jpg"  width="150" height="100" alt="ICE Logo"/>
			
			
		</div>
		</div>
		
		<!-- menu section -->
		
		
		<ul id="nav">
			<div id="side">
			<li><a href="index.php">Home</a> </li>
			<li><a href="link page/about.php">About ICE</a> </li>
			<li><a href="ice_session.php">Session</a>
				
			</li>
			
			<li><a href="teacher.php">Teachers</a>
				
			</li>
			<li><a href="#">Research</a> 
				<ul class="subs">
					<li><a href="link page/thesis.php">Thesis Area</a></li>
					<li><a href="link page/project.php">Project Area</a></li>

				</ul>
			</li>
			<li><a href="result.php">Result</a> </li>
			<li><a href="link page/facility.php">Facilities</a> </li>
			<li><a href="link page/galary.php">Gallary</a></li> 
			</div>
		</ul>				
		</div> <!-- end menu section -->
		</body>